#Copyrighted  burakgon - PITTECH 2013 
#This script getting a screenshot from an Android device and putting it to your desktop.
#How to use
#1- Connect your phone to computer
#2- Open the USB Debugging
#3- Run this script to get screenshots from your device to your desktop directly

#Ekran görüntüsünü cihaza ss.png ismiyle kaydeder.
adb shell screencap -p /sdcard/ss.png

#picdir isimli bi kisaltma tanimladik
picdir=~/Desktop/GetSS_PITscripts

#yukarda tanimladigim picdir isimli dosya yolunu mkdir ile olusturup cd ile icine girdim
mkdir -p $picdir;cd $picdir

#adb pull komutu ile dosyayı cihazdan aldım ve aldığım zamanın tarihini dosya adı olarak kaydettim
adb pull /sdcard/ss.png $picdir/`date +%h%m%s`.png

#cihazdaki ss.png yi sildimki yeni ekran goruntusu alabileyim
adb shell rm /sdcard/ss.png