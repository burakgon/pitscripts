# Copyright burakgon - PITTECH 2013
# How to use:
# 1- Create a file named as "ip" on your Dropbox folder to get your IP adress to your Dropbox folder back. 
# 2- This script will automatically bring your ip adress to a txt file on your Dropbox.
# You can learn your computers IP from anywhere, anytime!
#
#!/bin/bash
echo IP to Dropbox listener
while :
do
if [ -f ~/Dropbox/ip ]; then
date
echo "IP REQUEST FOUND"
curl -s icanhazip.com > ~/Dropbox/yourIP.txt
echo "Your IP is:"
cat ~/Dropbox/yourIP.txt
rm ~/Dropbox/ip
fi
sleep 1
done